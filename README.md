# Docker Example - Use [s3fuse] volume in containers

## Prerequisites

#### Set the default GCP project:

Edit envs file, setup following files:

```
SEC_PATH=${HOME}/.aws
AWS_PROFILE=<your aws profile>
AWS_REGION=us-west-1
BUCKET=<your example bucket name>
MOUNT_POINT=/mnt/s3fs
```

#### Create the bucket

Before invoking s3fuse, you must have a AWS bucket that you want to mount. If
your bucket doesn't yet exist, create one:

```
$ make bucket
```

## Build the s3fuse docker image

```
$ make build
```

## See it works

```
$ make up follow
...
producer_1  | message created
consumer_1  | total 1
consumer_1  | -rw-r--r-- 1 root root   0 May  1 00:34 index.html
consumer_1  | -rw-r--r-- 1 root root 174 May  2 03:08 message
consumer_1  | -rw-r--r-- 1 root root   0 May  1 01:15 test.html
consumer_1  | ======================================================
consumer_1  | This is a message from producer on Tue May  2 03:08:19 UTC 2017
consumer_1  | ======================================================
...
```

Go to [AWS Console] upload/delete some files to see changes in local logs.

## Shut it down

```
$ make down
```

[AWS Console]: https://console.aws.amazon.com
[s3fuse]: https://github.com/s3fs-fuse/s3fs-fuse
[console]: https://console.aws.amazon.com
[aws cli]: https://github.com/aws/aws-cli


