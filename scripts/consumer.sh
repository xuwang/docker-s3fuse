#!/bin/sh

while true
do
  until [ -f "${MOUNT_POINT}/message" ]
  do
    echo wait for producer
    sleep 2
  done
  ls -l ${MOUNT_POINT}
  cat ${MOUNT_POINT}/message
  sleep 5
done