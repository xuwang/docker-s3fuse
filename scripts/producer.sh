#!/bin/sh

while true
do
	cat <<EOF >${MOUNT_POINT}/message
======================================================
This is a message from producer on $(date)
======================================================
EOF
	echo message created
	sleep 5
done