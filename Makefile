include envs
AWSACCESSKEYID := $(shell cat ${SEC_PATH}/key-id.key)
AWSSECRETACCESSKEY := $(shell cat ${SEC_PATH}/access.key)
export

help: ## this info
	@# adapted from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo '_________________'
	@echo '| Make targets: |'
	@echo '-----------------'
	@cat Makefile | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build gcsfuse image
	docker-compose build

up: ## bring system up, see docker-compose.yml
	@if ! docker images s3fuse | grep -q s3fuse ; \
	then \
		make build; \
	fi
	docker-compose up -d --force-recreate --remove-orphans --build

down: ## bring system dev-down, see docker-compose.yml
	docker-compose down --remove-orphans --volumes

restart: ## restart test system
	docker-compose restart

follow: ## docker-compose logs -f
	docker-compose logs -f

prune: ## prune local docker containers and images
	docker container prune -f
	docker image prune -f
	docker volume prune -f

auth:

bucket: auth ## Create the bucket if not exists
	@if ! aws s3 ls --profile ${AWS_PROFILE} s3://${BUCKET} &> /dev/null; \
	then \
		echo creating s3://${BUCKET} ... ; \
		aws s3 mb s3://${BUCKET} --profile ${AWS_PROFILE} --region ${AWS_REGION} ; \
		sleep 10; \
	fi

destroy-bucket: auth ## Destroy the bucket if exists
	@if aws s3 ls --profile ${AWS_PROFILE} s3://${BUCKET} &> /dev/null; \
	then \
		echo destroy s3://${BUCKET} ... ; \
		aws s3 rb s3://${BUCKET} --profile ${AWS_PROFILE} --force ; \
	fi

sync: auth ## Rsync the gcs bucket
		aws s3 sync ./bucket s3://${BUCKET} --profile ${AWS_PROFILE} \
		--delete --storage-class REDUCED_REDUNDANCY

sync-docker-time: ## sync docker vm time with hardware clock
	@docker run --rm --privileged alpine hwclock -s

.PHONY: build prune push pull help up down restart follow
.PHONY: bucket destroy-bucket sync-docker-time